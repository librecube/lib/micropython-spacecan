import pyb
import micropython
import spacecan
import spacecan.services


micropython.alloc_emergency_exception_buf(100)
led_heartbeat = pyb.LED(1)


# define callback functions


def packet_monitor(service, subtype, data, node_id):
    if data:
        print(f"TM[{service:02}, {subtype:02}] with data {data} from node {node_id}")
    else:
        print(f"TM[{service:02}, {subtype:02}] with no data from node {node_id}")


def received_parameter_value_report(node_id, report):
    print(f"--> received parameter value report from node {node_id}:")
    for parameter_id, value in report.items():
        parameter = pus.parameter_management.get_parameter(parameter_id)
        print(f" => {parameter.parameter_name}: {value}")


def received_housekeeping_report(node_id, report_id, report):
    print(f"--> received housekeeping report {report_id} from node {node_id}:")
    for parameter_id, value in report.items():
        parameter = pus.parameter_management.get_parameter(parameter_id)
        print(f" => {parameter.parameter_name}: {value}")


# create node and configure packet utilization protocol


controller = spacecan.Controller.from_file("config/controller.json")
controller.sent_heartbeat = led_heartbeat.toggle
pus = spacecan.services.PacketUtilizationServiceController(controller)

pus.packet_monitor = packet_monitor
pus.parameter_management.add_parameters_from_file("config/parameters.json", node_id=1)
pus.parameter_management.add_parameters_from_file("config/parameters.json", node_id=2)
pus.housekeeping.add_housekeeping_reports_from_file(
    "config/housekeeping.json", node_id=1
)
pus.housekeeping.add_housekeeping_reports_from_file(
    "config/housekeeping.json", node_id=2
)
pus.function_management.add_functions_from_file("config/functions.json", node_id=1)
pus.function_management.add_functions_from_file("config/functions.json", node_id=2)
pus.test.received_connection_test_report = lambda node_id: print(
    f"--> received connection test report from {node_id}",
)
pus.parameter_management.received_parameter_value_report = (
    received_parameter_value_report
)
pus.housekeeping.received_housekeeping_report = received_housekeeping_report

controller.connect()
controller.start()

INTRO_TEXT = """
    <Stop program with Ctrl-C>

    Enter commands using this pattern:   
        <n>,<s>,<t>,<data>
    
    where:
        n = node id
        s = service
        t = subtype
        data are comma separated byte values, optional

        Example: 2,17,1 sends a test command (17,1) to node 2

    Enter 'h' to show this text again.
"""
print(INTRO_TEXT)

try:
    while True:
        x = input("").lower()

        if x == "h":
            print(INTRO_TEXT)

        else:
            try:
                x = x.split(",")
                node_id = int(x[0])
                service = int(x[1])
                subtype = int(x[2])

            except ValueError as e:
                print("invalid input")
                continue

            case = (service, subtype)

            # enable periodic housekeeping reports
            if case == (3, 5):
                report_ids = [int(i) for i in x[3:]]
                pus.housekeeping.send_enable_period_housekeeping_reports(
                    node_id, report_ids
                )

            # disable periodic housekeeping reports
            elif case == (3, 6):
                report_ids = [int(i) for i in x[3:]]
                pus.housekeeping.send_disable_period_housekeeping_reports(
                    node_id, report_ids
                )

            # request single shot housekeeping reports
            elif case == (3, 27):
                report_ids = [int(i) for i in x[3:]]
                pus.housekeeping.send_single_shot_housekeeping_reports(
                    node_id, report_ids
                )

            # perform a function
            elif case == (8, 1):
                function_id = int(x[3])
                arguments = [int(y) for y in x[4:]] if len(x) > 4 else None
                pus.function_management.send_perform_function(
                    node_id, function_id, arguments
                )

            # connection test
            elif case == (17, 1):
                pus.test.send_connection_test(node_id)

            # application connection test
            elif case == (17, 3):
                apid = int(x[3])
                pus.test.send_application_connection_test(node_id, apid)

            # report parameter values
            elif case == (20, 1):
                parameter_ids = [int(i) for i in x[3:]]
                pus.parameter_management.send_report_parameter_values(
                    node_id, parameter_ids
                )

except KeyboardInterrupt:
    print()

controller.stop()
controller.disconnect()
