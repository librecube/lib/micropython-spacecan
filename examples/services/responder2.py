import time
import random
import pyb
import micropython
import spacecan
import spacecan.services


micropython.alloc_emergency_exception_buf(100)
led_heartbeat = pyb.LED(1)


# map parameter alias to parameter ids
PARAM_COUNTER = 1
PARAM_SWITCH = 2
PARAM_TEMPERATURE = 3
PARAM_VOLTAGE = 4

# map function alias to function ids
FUNC_RESET_COUNTER = 1
FUNC_SET_SWITCH = 2


# define callback functions


def packet_monitor(service, subtype, data, node_id):
    if data:
        print(f"TC[{service:02}, {subtype:02}] with data {data} to node {node_id}")
    else:
        print(f"TC[{service:02}, {subtype:02}] with no data to node {node_id}")


def perform_function(function_id, arguments):
    if function_id == FUNC_RESET_COUNTER:
        set_param(PARAM_COUNTER, 0)
    elif function_id == FUNC_SET_SWITCH:
        set_param(PARAM_SWITCH, arguments[0])


# create node and configure packet utilization protocol


responder = spacecan.Responder.from_file("config/responder2.json")
responder.received_heartbeat = led_heartbeat.toggle
pus = spacecan.services.PacketUtilizationServiceResponder(responder)

pus.packet_monitor = packet_monitor
pus.parameter_management.add_parameters_from_file("config/parameters.json")
pus.housekeeping.add_housekeeping_reports_from_file("config/housekeeping.json")
pus.function_management.add_functions_from_file("config/functions.json")
pus.function_management.perform_function = perform_function

set_param = pus.parameter_management.set_parameter_value
get_param = pus.parameter_management.get_parameter_value

responder.connect()
responder.start()

try:
    while True:
        # do some other stuff
        time.sleep(0.1)

        # update housekeeping data
        set_param(PARAM_COUNTER, get_param(PARAM_COUNTER) + 1)
        set_param(PARAM_TEMPERATURE, 25 + random.random())
        if get_param(PARAM_SWITCH):
            set_param(PARAM_VOLTAGE, 5 + random.random())
        else:
            set_param(PARAM_VOLTAGE, 0)

except KeyboardInterrupt:
    pass

responder.stop()
responder.disconnect()
