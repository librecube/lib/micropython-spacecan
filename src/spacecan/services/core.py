import micropython

from .ST01_request_verification import (
    RequestVerificationServiceController,
    RequestVerificationServiceResponder,
)
from .ST03_housekeeping import (
    HousekeepingServiceController,
    HousekeepingServiceResponder,
)
from .ST08_function_management import (
    FunctionManagementServiceController,
    FunctionManagementServiceResponder,
)
from .ST17_test import TestServiceController, TestServiceResponder
from .ST20_parameter_management import (
    ParameterManagementServiceController,
    ParameterManagementServiceResponder,
)


class PacketUtilizationService:
    def __init__(self):
        self.packet_monitor = None
        self.request_verification = None
        self.housekeeping = None
        self.function_management = None
        self.test = None
        self.parameter_management = None


class PacketUtilizationServiceController(PacketUtilizationService):
    def __init__(self, parent):
        self.parent = parent
        self.parent.received_packet = self.received_packet

        self.request_verification = RequestVerificationServiceController(self)
        self.housekeeping = HousekeepingServiceController(self)
        self.function_management = FunctionManagementServiceController(self)
        self.test = TestServiceController(self)
        self.parameter_management = ParameterManagementServiceController(self)

        self.packet_monitor = None

    def send(self, packet, node_id):
        try:
            self.parent.send_packet(packet, node_id)
        except AttributeError:
            # catch the situation where network is not yet initialized
            pass

    def received_packet(self, data, node_id):
        service = data[0]
        subtype = data[1]
        data = data[2:]

        if self.packet_monitor is not None:
            self.packet_monitor(service, subtype, data, node_id)

        # dispatch packet to the individual service handlers
        # run them in threads to not block the main loop

        # request verification service
        if service == 1:
            micropython.schedule(
                RequestVerificationServiceController.process,
                (self.request_verification, service, subtype, data, node_id),
            )

        # housekeeping service
        elif service == 3:
            micropython.schedule(
                HousekeepingServiceController.process,
                (self.housekeeping, service, subtype, data, node_id),
            )

        # function management service
        elif service == 8:
            pass  # not applicable

        # test service
        elif service == 17:
            micropython.schedule(
                TestServiceController.process,
                (self.test, service, subtype, data, node_id),
            )

        # parameter management service
        elif service == 20:
            micropython.schedule(
                ParameterManagementServiceController.process,
                (self.parameter_management, service, subtype, data, node_id),
            )


class PacketUtilizationServiceResponder(PacketUtilizationService):
    def __init__(self, parent):
        self.parent = parent
        self.parent.received_packet = self.received_packet

        self.request_verification = RequestVerificationServiceResponder(self)
        self.housekeeping = HousekeepingServiceResponder(self)
        self.function_management = FunctionManagementServiceResponder(self)
        self.test = TestServiceResponder(self)
        self.parameter_management = ParameterManagementServiceResponder(self)

        self.packet_monitor = None

    def send(self, packet):
        try:
            self.parent.send_packet(packet)
        except AttributeError:
            # catch the situation where network is not yet initialized
            pass

    def received_packet(self, data, node_id):
        service = data[0]
        subtype = data[1]
        data = data[2:]

        if self.packet_monitor is not None:
            self.packet_monitor(service, subtype, data, node_id)

        # dispatch packet to the individual service handlers
        # run them in threads to not block the main loop

        # request verification service
        if service == 1:
            micropython.schedule(
                RequestVerificationServiceResponder.process,
                (self.request_verification, service, subtype, data, node_id),
            )

        # housekeeping service
        elif service == 3:
            micropython.schedule(
                HousekeepingServiceResponder.process,
                (self.housekeeping, service, subtype, data, node_id),
            )

        # function management service
        elif service == 8:
            micropython.schedule(
                FunctionManagementServiceResponder.process,
                (self.function_management, service, subtype, data, node_id),
            )

        # test service
        elif service == 17:
            micropython.schedule(
                TestServiceResponder.process,
                (self.test, service, subtype, data, node_id),
            )

        # parameter management service
        elif service == 20:
            micropython.schedule(
                ParameterManagementServiceResponder.process,
                (self.parameter_management, service, subtype, data, node_id),
            )
